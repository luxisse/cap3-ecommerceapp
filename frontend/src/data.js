const data = {
    products: [

        {
            name: 'Chocolate Mint',
            slug: 'choc-mint',
            category: 'Specials',
            image: '/images/ChocMint.jpeg',
            price: 420,
            countInStock: 35,
            rating: 3.0,
            numReviews: 3,
            description: 'Real fresh mint gelato with chocolate chip'
        },

        {
            name: 'Cookies & Cream',
            slug: 'cookies-cream',
            category: 'Classics',
            image: '/images/CookiesC.jpeg',
            price: 370,
            countInStock: 11,
            rating: 4.0,
            numReviews: 6,
            description: 'Light cocoa gelato with crumbled cookies'
        },

        {
            name: 'Berry Cheesecake',
            slug: 'berry',
            category: 'Classics',
            image: '/images/Berry.jpeg',
            price: 365,
            countInStock: 24,
            rating: 3.0,
            numReviews: 9,
            description: 'Cheddar cheese ice cream with berry swirls and chunks of cracker crust'
        },

        {
            name: 'Toffee Almond Crunch',
            slug: 'toffee-almond',
            category: 'Classics',
            image: '/images/TAlmond.jpeg',
            price: 420,
            countInStock: 10,
            rating: 5.0,
            numReviews: 14,
            description: 'Caramel toffee mixed with coffee base, added with crunchy almond pralines'
        },

        {
            name: 'Tiramisu',
            slug: 'tiramisu',
            category: 'Classics',
            image: '/images/Tiramisu.jpeg',
            price: 480,
            countInStock: 19,
            rating: 5.0,
            numReviews: 22,
            description: 'Egg, marsala and mascarpone gelato with lady finger biscuits'
        },
        
        {
            name: 'Salted Caramel Coco Sugar',
            slug: 'salted-caramel',
            category: 'Specials',
            image: '/images/SCaramel.jpeg',
            price: 420,
            countInStock: 10,
            rating: 4.5,
            numReviews: 10,
            description: 'Caramelized coconut sugar mixed with our rich cream base and topped with sea salt make up the perfect blend of butterscotch flavor with a hint of saltiness'
        },

        {
            name: 'Double Dutch',
            slug: 'double-dutch',
            category: 'Classics',
            image: '/images/DDutch.jpeg',
            price: 400,
            countInStock: 7,
            rating: 5.0,
            numReviews: 12,
            description: 'Homemade almond caramel candy mixed with cacao nibs and brownies'
        },

        {
            name: 'Mango',
            slug: 'mango',
            category: 'Classics',
            image: '/images/Mango.jpeg',
            price: 400,
            countInStock: 0,
            rating: 4.0,
            numReviews: 5,
            description: 'Mango sorbet made with fresh local mangoes'
        }
    ]
};

export default data;