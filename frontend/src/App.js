import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';
import { useContext, useEffect, useState } from 'react';
import { Store } from './Store';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Home from "./pages/Home";
import Product from "./pages/Product";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Badge from 'react-bootstrap/Badge';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Cart from './pages/Cart';
import Login from './pages/Login';
import ProductEdit from './pages/ProductEdit';
import ShippingDetails from './pages/ShippingDetails';
import Register from './pages/Register';
import PaymentMethod from './pages/PaymentMethod';
import ProductList from './pages/ProductList';
import { getError } from './utils';
import axios from 'axios';
import SearchBox from './components/SearchBox';
import AdminRoute from './components/AdminRoute';
import ProtectedRoute from './components/ProtectedRoute';



function App() {
  const { state, dispatch: ctxDispatch } = useContext(Store);
  const { cart, userInfo } = state;

  const logoutHandler = () => {
    ctxDispatch({ type: 'USER_LOGOUT' });
    localStorage.removeItem('userInfo');
    localStorage.removeItem('shippingDetails');
    localStorage.removeItem('paymentMethod');
  };

  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    const fetchCategories = async () => {
      
      try {
        const { data } = await axios.get(`/api/products/categories`);
        setCategories(data);

      } catch(err) {
        toast.error(getError(err));
      }
    };
    fetchCategories();
  }, []);
  
  return (
    <BrowserRouter>
    <div className= {
      sidebarOpen
      ? "d-flex flex-column site-container active-cont" 
      : "d-flex flex-column site-container" 
      }
    >

    {/* custom notifications */}
    <ToastContainer position='top-center' limit={1} />

      <header>
        <Navbar className="color-header" variant="dark">
          <Container>
              <Button
                variant="dark"
                onClick={() => setSidebarOpen(!sidebarOpen)}
                className = "sidebar-btn"
              >
                <i className="fas fa-bars"></i>
              </Button>

            <LinkContainer to="/">
              <Navbar.Brand className="app-title">Two Spoons Creamery</Navbar.Brand>
            </LinkContainer> 

            <Navbar.Toggle aria-controls='basic-navbar-nav' />

           
            <Navbar.Collapse className='mx-5' id='basic-navbar-nav'>
              
              <SearchBox />
            
              <Nav className="me-auto w-100 justify-content-end">
                <Link to = "/cart" className="nav-link">
                  Cart
                  {cart.cartItems.length > 0 && (
                    <Badge pill bg="danger">
                      {cart.cartItems.reduce((a, c) => a + c.quantity, 0)}
                    </Badge>
                  )}
                </Link>

                  {userInfo ? (
                    // if user is already logged in
                    <NavDropdown title = {userInfo.name} id = "basic-nav-dropdown">

                      {/* Logout link for logged-in users */}
                        <Link 
                          className="dropdown-item" 
                          to="/" 
                          onClick={logoutHandler}>
                            Log Out
                        </Link>
                      </NavDropdown>

                  ):(
                    
                    // Login link for users not logged in
                    <Link className="nav-link" to="/login">
                    Log In
                    </Link>
                  )}

                  {/* Admin user functionalities */}
                  {userInfo && userInfo.isAdmin && (
                    <NavDropdown title="Your dashboard" id="admin-nav-dropdown">
       
                      <LinkContainer to="/admin/products">
                        <NavDropdown.Item>Manage products</NavDropdown.Item>
                      </LinkContainer>

                    </NavDropdown>
                  )}

                    {/* <Link className="nav-link" to="/register">
                    Register
                    </Link> */}
                
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </header>
      
      {/* sidebar nav */}
      <div
          className={
            sidebarOpen
              ? 'active-nav side-navbar d-flex justify-content-between flex-wrap flex-column'
              : 'side-navbar d-flex justify-content-between flex-wrap flex-column'
          }
        >
          {/* nav elements */}
          <Nav className="flex-column text-brown w-100 p-2 nav-elem">
            <Nav.Item className='p-label'>
              <strong>Categories</strong>
            </Nav.Item>
            {categories.map((category) => (
              <Nav.Item key={category}>
                <LinkContainer
                  to={`/search?category=${category}`}
                  onClick={() => setSidebarOpen(false)}
                >
                  <Nav.Link>{category}</Nav.Link>
                </LinkContainer>
              </Nav.Item>
            ))}
          </Nav>
        </div>

      <main>
        <Container className="mt-3">

          {/* Routes */}
          <Routes>
            <Route path="/product/:slug" element={<Product />} />
            <Route 
              path="/cart" 
              element={
                <ProtectedRoute>
                  <Cart />
                </ProtectedRoute>
              } /> 
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/shipping" element={<ShippingDetails />} />
            <Route path="/payment" element={<PaymentMethod />} />
            
            {/* Admin Routes */}
            <Route
                path="/admin/products"
                element={
                  <AdminRoute>
                    <ProductList />
                  </AdminRoute>
                }
              ></Route>

              <Route
                path="/admin/products/:id"
                element={
                  <AdminRoute>
                    <ProductEdit />
                  </AdminRoute>
                }
              ></Route>

            <Route path="/" element={<Home />} />
          </Routes>

        </Container>
      </main>

      <footer className="footer">
        <div className="text-center">©Two Spoons 2022. All rights reserved.</div>
      </footer>

    </div>
    </BrowserRouter>
  );
}

export default App;