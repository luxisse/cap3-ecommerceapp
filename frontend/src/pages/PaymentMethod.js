import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Helmet } from "react-helmet-async";
import { Store } from "../Store";
import Form from 'react-bootstrap/Form';
import Button from "react-bootstrap/Button";



export default function PaymentMethod() {
    
    const navigate = useNavigate();
    const { state, dispatch : ctxDispatch } = useContext(Store);
    const {
        cart: { shippingDetails, paymentMethod }
    } = state;

    const [paymentMethodName, setPaymentMethod] = useState(
        paymentMethod || "GCash"
    );

    useEffect(() => {
        if (!shippingDetails.address) {
            navigate('/shipping');
        }
    }, [shippingDetails, navigate]);

    const submitHandler = (e) => {
        e.preventDefault();
        ctxDispatch({type: 'SAVE_PAYMENT_METHOD', payload: paymentMethodName});
        // save to local storage
        localStorage.setItem('paymentMethod', paymentMethodName);
        navigate('/');
    };
    
    return <div>
        <div className="container small-container">
            <Helmet>
                <title>
                    Payment
                </title>
            </Helmet>

                <h1 className="our-products">Your Payment Method</h1>

                {/* start of payment form */}
                <Form className="div-form" onSubmit = {submitHandler}>
                    <div className="mb-3">
                        {/* GCash option */}
                        <Form.Check
                            type = "radio"
                            id = "GCash"
                            label = "GCash"
                            value = "GCash"
                            checked = {paymentMethodName === 'GCash'}
                            onChange = {(e) => setPaymentMethod(e.target.value)}
                        />
                    </div>

                    <div className="mb-3">
                        {/* PayPal option */}
                        <Form.Check
                            type = "radio"
                            id = "PayPal"
                            label = "PayPal"
                            value = "PayPal"
                            checked = {paymentMethodName === 'PayPal'}
                            onChange = {(e) => setPaymentMethod(e.target.value)}
                        />
                    </div>

                    <div className="mb-5">
                        {/* Mastercard option */}
                        <Form.Check
                            type = "radio"
                            id = "Mastercard"
                            label = "Mastercard"
                            value = "Mastercard"
                            checked = {paymentMethodName === 'VMastercard'}
                            onChange = {(e) => setPaymentMethod(e.target.value)}
                        />
                    </div>

                    {/* form btn */}
                    <div>
                        <Button className="form-btn" type="submit">Checkout</Button>
                    </div>

                </Form>
            
        </div>
    </div>;
};