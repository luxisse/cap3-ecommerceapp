import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Helmet } from "react-helmet-async";
import { Store } from '../Store';
import Form from 'react-bootstrap/Form';
import Button from "react-bootstrap/Button";



export default function ShippingDetails() {
    
    const navigate = useNavigate();
    const { state, dispatch: ctxDispatch } = useContext(Store);

    const {
        userInfo,
        cart: { shippingDetails }
    } = state;

    const [fullName, setFullName] = useState(shippingDetails.fullName || '');
    const [address, setAddress] = useState(shippingDetails.address || '');
    const [city, setCity] = useState(shippingDetails.city || '');
    const [postCode, setPostCode] = useState(shippingDetails.postCode || '');
    const [country, setCountry] = useState(shippingDetails.country || '');

    // if user isn't logged in, redirect to Log In page
    useEffect(() => {
        if (!userInfo) {
            navigate('/login?redirect=/shipping');
        }
    }, [userInfo, navigate]);

    const submitHandler = (e) => {
        e.preventDefault();
        ctxDispatch({
            type: 'SAVE_SHIPPING_DETAILS',
            payload: {
                fullName,
                address,
                city,
                postCode,
                country
            }
        });
        localStorage.setItem(
            'shippingDetails',
            JSON.stringify({
                fullName,
                address,
                city,
                postCode,
                country
            })
        );
        navigate('/payment');
    };
    
    
    return <div>
        <Helmet>
            <title>Shipping Details</title>
        </Helmet>

        <h1 className="our-products">Your Shipping Details</h1>
        <h5 className="h5-subtitle">Add the address where we'll send your order</h5>

        <div className="container small-container">
        <Form onSubmit={submitHandler}>
            
            {/* Full Name field */}
            <Form.Group className = "mb-3" controlId = "fullName">
                <Form.Label>Full Name</Form.Label>
                <Form.Control
                    value = {fullName}
                    onChange = {(e) => setFullName(e.target.value)}
                    required
                />
            </Form.Group>

             {/* Address field */}
             <Form.Group className = "mb-3" controlId = "address">
                <Form.Label>Address</Form.Label>
                <Form.Control
                    value = {address}
                    onChange = {(e) => setAddress(e.target.value)}
                    required
                />
            </Form.Group>

            {/* City field */}
            <Form.Group className = "mb-3" controlId = "city">
                <Form.Label>City</Form.Label>
                <Form.Control
                    value = {city}
                    onChange = {(e) => setCity(e.target.value)}
                    required
                />
            </Form.Group>

            {/* Post code field */}
            <Form.Group className = "mb-3" controlId = "postCode">
                <Form.Label>Post Code</Form.Label>
                <Form.Control
                    value = {postCode}
                    onChange = {(e) => setPostCode(e.target.value)}
                    required
                />
            </Form.Group>

            {/* Country field */}
            <Form.Group className = "mb-5" controlId = "country">
                <Form.Label>Country</Form.Label>
                <Form.Control
                    value = {country}
                    onChange = {(e) => setCountry(e.target.value)}
                    required
                />
            </Form.Group>

            {/* submit button */}
            <div className="mb-3">
                <Button className="form-btn" variant = "primary" type="submit">
                    Continue
                </Button>
            </div>
        </Form>
        </div>

    </div>;
};